+++
title = 'Risotto Funghi'
date = 2024-10-11T14:19:13-03:00
draft = false
+++

## Ingredientes

- 1 ½ xícara de arroz arbóreo
- 500 g de cogumelos frescos e fatiados
- 2 dentes de alho, picados
- 4 xícaras de caldo de legumes
- 1/2 xícara de vinho branco seco
- 1/2 xícara de queijo parmesão ralado
- 2 colheres de sopa de manteiga
- 2 colheres de sopa de azeite de oliva
- Sal e pimenta-do-reino a gosto

## Modo de Preparo

1. Aqueça o caldo de legumes em uma panela
2. Em uma frigideira grande, adicione 1 colher de sopa de azeite de oliva e 1 colher de sopa de manteiga.
3. Adicione os cogumelos fatiados e refogue até que fiquem macios e dourados.
4. Tempere com sal e pimenta e reserve
5. Na mesma frigideira, adicione mais 1 colher de sopa de azeite e a cebola picada.
6. Adicione o alho picado e refogue por mais 1-2 minutos
7. Adicione o arroz arbóreo à frigideira e misture bem
8. adicione o vinho branco e cozinhe até que o líquido evapore, mexendo sempre
9. Comece a adicionar o caldo quente, uma concha de cada vez, mexendo constantemente
10. Aguarde até que o líquido seja absorvido antes de adicionar mais caldo. Repita esse processo por cerca de 18-20 minutos, até que o arroz esteja al dente e cremoso
11. Adicione os cogumelos refogados, o queijo parmesão ralado e o restante da manteiga
12. Ajuste o tempero com sal e pimenta e sirva
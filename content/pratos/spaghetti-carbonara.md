+++
title = 'Spaghetti Carbonara'
date = 2024-10-11T14:19:31-03:00
draft = false
+++

## Ingredientes

- 400g de spaghetti
- 150g de pancetta, cortada em cubos
- 3 ovos grandes
- 100g de queijo parmesão ralado
- 2 dentes de alho, picados
- Sal a gosto
- Pimenta do reino a gosto 

## Modo de Preparo

1. Cozinhe o spaghetti e reserve 1/2 xícara da água do 
cozimento
2. bata os ovos e adicione o queijo parmesão ou pecorino ralado, misture bem até virar uma pasta e tempere
3. Em uma frigideira grande, adicione a pancetta e cozinhe em fogo médio até que fique crocante 
4. Quando a pancetta estiver pronta, adicione o spaghetti escorrido à frigideira e misture bem
5. Adicione a mistura de ovos e queijo imediatamente, mexendo rapidamente para que o calor da massa cozinhe os ovos, formando um molho cremoso
6. Adicione vinagre balsâmico
7. Sirva imediatamente, polvilhando com mais queijo parmesão e pimenta-do-reino a gosto.
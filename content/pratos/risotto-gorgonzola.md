+++
title = 'Risotto Gorgonzola'
date = 2024-10-11T13:23:32-03:00
draft = false
+++

## Ingredientes

- 1 ½ xícara de arroz arbóreo
- 1 cebola média picada
- 2 dentes de alho picados
- 4 xícaras de caldo de legumes
- 1/2 xícara de vinho branco seco
- 150 g de queijo gorgonzola, cortado em pedaços pequenos
- 2 colheres de sopa de manteiga
- 1/2 xícara de queijo parmesão ralado
- Sal e pimenta-do-reino a gosto
- Nozes picadas

## Modo de Preparo

1. Aqueça o caldo de legumes em uma panela
2. Em uma frigideira grande, derreta uma colher de sopa de manteiga e adicione a cebola picada. Refogue até que fique transparente.
3. Adicione o alho picado e refogue por mais 1-2 minutos
4. Adicione o arroz arbóreo à frigideira e misture bem para que os grãos fiquem envolvidos na manteiga. Refogue por cerca de 2 minutos, até que o arroz fique levemente translúcido.
5. Adicione o vinho branco e cozinhe até que o líquido evapore, mexendo sempre.
6. Comece a adicionar o caldo quente, uma concha de cada vez, mexendo constantemente. Aguarde até que o líquido seja absorvido antes de adicionar mais caldo. Repita esse processo por cerca de 18-20 minutos
7. Quando o arroz estiver quase pronto, adicione o queijo gorgonzola e misture até que derreta completamente. Adicione o queijo parmesão e a outra colher de sopa de manteiga, mexendo bem para incorporar
8. Tempere com sal e pimenta a gosto
9. Acrescente as nozes picadas e sirva
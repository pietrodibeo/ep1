+++
title = 'Pastiera Di Grano'
date = 2024-10-11T13:24:25-03:00
draft = false
+++

## Ingredientes

- 300 g de farinha de trigo
- 150 g de açúcar
- 150 g de manteiga
- 1 ovo
- 1 gema
- Raspas de 1 limão
- 250 g de grãos de trigo cozidos
- 200 ml de leite
- 30 g de manteiga
- 1 colher de chá de raspas de laranja
- 400 g de ricota (drenada)
- 300 g de açúcar
- 4 ovos
- 1 colher de chá de essência de baunilha
- 100 g de frutas cristalizadas (picadas)
- 1 colher de chá de canela em pó
- Raspas de 1 limão e 1 laranja

## Modo de Preparo

1. Em uma tigela grande, misture a farinha e o açúcar
2. Adicione a manteiga e misture com as pontas dos dedos até obter uma farofa
3. Acrescente o ovo, a gema e as raspas de limão. Misture até formar uma massa homogênea
4. Envolva a massa em plástico filme e leve à geladeira por pelo menos 30 minutos
5. Em uma panela, cozinhe o trigo cozido com o leite, a manteiga e as raspas de laranja, mexendo em fogo médio por cerca de 10-15 minutos, até que o trigo fique bem macio e absorva o leite. Deixe esfriar
6. Em uma tigela separada, bata a ricota com o açúcar até obter um creme liso
7. Adicione os ovos, a essência de baunilha, as frutas cristalizadas, a canela, as raspas de limão e laranja e misture bem
8. Acrescente o trigo cozido e mexa delicadamente para incorporar
9. Preaqueça o forno a 180°C
10. Abra a massa em uma superfície enfarinhada e forre uma forma de torta (de preferência de aro removível) com a massa, deixando um pouco de massa para fazer tiras decorativas
11. Despeje o recheio de trigo e ricota na forma
12. Com a massa restante, corte tiras e coloque-as em forma de grade sobre o recheio
13. Leve ao forno e asse por cerca de 1 hora, ou até que a pastiera esteja dourada e firme ao toque
14. Tradicionalmente, a pastiera é deixada repousar por um dia ou dois antes de servir, para que os sabores se misturem bem
15. Polvilhe açúcar de confeiteiro por cima antes de servir, se desejar
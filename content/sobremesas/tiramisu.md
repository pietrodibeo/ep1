+++
title = 'Tiramisù'
date = 2024-10-11T14:19:49-03:00
draft = false
+++

## Ingredientes

- 4 gemas de ovo
- 1/2 xícara de açúcar
- 1 xícara de queijo mascarpone
- 1 xícara de creme de leite fresco, batido em ponto de chantilly
- 1 xícara de café espresso forte, resfriado
- 1 colher de sopa de licor de café ou licor de amêndoas
- 1 pacote de biscoitos champagne
- Cacau em pó, para polvilhar

## Modo de Preparo

1. Em uma tigela, bata as gemas com o açúcar até obter um creme claro e fofo
2. Adicione o queijo mascarpone ao creme de gemas e açúcar e misture delicadamente até ficar homogêneo
3. Incorpore o chantilly, misturando suavemente para manter a textura leve e aerada. Reserve
4. Misture o café frio com o licor
5. Passe cada biscoito champagne rapidamente na mistura de café, apenas para umedecê-los levemente, tomando cuidado para que não fiquem muito encharcados
6. Em um refratário, faça uma camada de biscoitos champagne umedecidos
7. Cubra com uma camada do creme de mascarpone
8. Repita as camadas até que os ingredientes acabem, finalizando com uma camada de creme
9. Leve o tiramisù à geladeira por pelo menos 4 horas
10. Antes de servir, polvilhe o tiramisù com cacau em pó
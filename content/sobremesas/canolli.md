+++
title = 'Cannoli'
date = 2024-10-11T14:20:06-03:00
draft = false
+++

## Ingredientes

- 2 xícaras de farinha de trigo
- 1 colher de sopa de açúcar
- 1/4 colher de chá de sal
- 2 colheres de sopa de manteiga
- 1 ovo
- 1/4 xícara de vinho Marsala ou vinho tinto doce 
- Óleo
- 500 g de ricota fresca, drenada
- 1/2 xícara de açúcar de confeiteiro
- 1/2 xícara de gotas de chocolate
- 1/2 colher de chá de essência de baunilha
- Raspas de 1 limão
- Açúcar de confeiteiro

## Modo de Preparo

1. Em uma tigela, misture a farinha de trigo, o açúcar e o sal.
2. Adicione a manteiga e, com os dedos, misture até obter uma farofa
3. Em seguida, adicione o ovo e o vinho Marsala e misture até formar uma massa homogênea. A massa deve ser firme, mas maleáve
4. Sove a massa por alguns minutos e depois envolva em plástico filme. Deixe descansar por cerca de 30 minutos
5. Em uma superfície enfarinhada, abra a massa com um rolo até que fique bem fina
6. Corte a massa em círculos de aproximadamente 10 cm de diâmetro
7. Enrole cada círculo de massa em torno de moldes próprios para cannoli (cilindros de metal) e pressione as pontas para selar
8. Aqueça o óleo a 180°C. Frite os cilindros de massa até que fiquem dourados e crocantes
9. Retire do óleo e deixe escorrer sobre papel-toalha. Aguarde até que esfriem um pouco e retire cuidadosamente os moldes
10. Em uma tigela, misture a ricota, o açúcar de confeiteiro, a essência de baunilha e as raspas de limão, se estiver usando. Mexa até obter uma mistura cremosa e homogênea
11. Adicione as gotas de chocolate e misture
12. Com a ajuda de um saco de confeitar, recheie os tubos de massa frita com o creme de ricota. Recheie apenas na hora de servir
13. Polvilhe açúcar de confeiteiro por cima para decorar
14. Sirva os cannoli imediatamente, enquanto a casquinha ainda está crocante e o recheio, fresco
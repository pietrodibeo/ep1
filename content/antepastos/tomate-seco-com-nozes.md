+++
title = 'Tomate Seco Com Nozes'
date = 2024-10-11T13:23:00-03:00
draft = false
+++

## Ingredientes

- 500 g tomates italianos
- 1/2 xícara de nozes picadas
- 1/4 de xícara de azeite de oliva
- 2 dentes de alho picados
- 1 colher de chá de sal
- 1 colher de chá de açúcar
- 1 colher de chá de orégano seco
- Pimenta-do-reino a gosto
- Folhas de manjericão

## Modo de Preparo

1. Preaqueça o forno a 100°C
2. Lave os tomates e corte-os ao meio
3. Em uma tigela, misture os tomates cortados com o sal, açúcar, orégano, alho picado e pimenta-do-reino. Deixe marinar por 15 minutos.
4. Forre uma assadeira com papel manteiga e distribua os tomates na assadeira, com o lado cortado para cima.
5. Asse os tomates no forno por cerca de 2 a 3 horas, ou até que fiquem secos, mas ainda ligeiramente macios.
6. Coloque as nozes em uma frigideira seca e toste-as em fogo médio, mexendo frequentemente, até que fiquem levemente douradas e aromáticas
7. Quando os tomates estiverem prontos, retire-os do forno e deixe esfriar
8. Em uma tigela, misture os tomates secos, as nozes tostadas e o azeite de oliva. Ajuste o sal e a pimenta a gosto
9. Sirva com folhas de manjericão fresco, se desejar
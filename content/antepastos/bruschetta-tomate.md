+++
title = 'Bruschetta de Tomate'
date = 2024-10-11T14:18:09-03:00
draft = false
+++

## Ingredientes

- 1 baguete ou pão italiano
- 4 tomates maduros, picados em cubos
- 2 dentes de alho, picados finamente
- 1/4 de xícara de manjericão fresco, picado
- 1/4 de xícara de azeite de oliva
- Sal e pimenta-do-reino a gosto
- Queijo parmesão ralado (opcional, para servir)

## Modo de Preparo

1. Preaqueça o forno a 180°C.
2. Corte a baguete ou o pão italiano em fatias diagonais, com cerca de 1,5 cm de espessura.
3. Coloque as fatias em uma assadeira e regue com um fio de azeite.
4. Asse no forno por cerca de 5-7 minutos, ou até que fiquem douradas e crocantes. Reserve.
5. Em uma tigela, combine os tomates picados, o alho picado e o manjericão. Misture bem.
6. Adicione o azeite de oliva e tempere com sal e pimenta-do-reino a gosto. Misture novamente.
7. Retire as fatias de pão do forno e coloque uma porção generosa da mistura de tomate sobre cada fatia.
8. Se desejar, polvilhe queijo parmesão ralado por cima.
9. Volte as bruschettas ao forno por mais 2-3 minutos, apenas para aquecer a mistura de tomate (opcional).
10. Sirva imediatamente como aperitivo ou acompanhamento.
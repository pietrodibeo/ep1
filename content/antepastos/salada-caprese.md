+++
title = 'Salada Caprese'
date = 2024-10-11T14:18:51-03:00
draft = false
+++

## Ingredientes

- 4 tomates italianos maduros
- 250 g de queijo mozzarella de búfala
- Folhas frescas de manjericão (a gosto)
- Azeite de oliva extra-virgem (a gosto)
- Vinagre balsâmico (opcional)
- Sal e pimenta-do-reino a gosto

## Modo de Preparo

1. Corte os tomates em rodelas
2. Corte o quejo em fatias
3. Misture o tomate e o queijo
4. Insira as folhas de manjericão
5. Regue com azeite de oliva a gosto
6. Adicione vinagre balsâmico
7. Tempere com sal e pimenta-do-reino a gosto